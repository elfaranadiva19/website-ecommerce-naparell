<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;

class AboutUserController extends Controller
{
     public function about()
    {
        $data['cart'] = Cart::count();
        return view('user/about', $data);
    }
}
