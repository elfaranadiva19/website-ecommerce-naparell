<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;

class ContactUserController extends Controller
{
       public function contact()
    {
        $data['cart'] = Cart::count();
        return view('user/contact', $data);
    }
}

