<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Produk;
use App\Checkout;

class ChekController extends Controller
{
    public function formcheckout()
    {
    	$data['produk'] = \DB::table('t_cart')->get();
    	$data['subtotal'] = \DB::table('t_cart')->sum('harga');
    	$data['cart'] = Cart::count();
        return view('user/formcheckout', $data);
    }

     public function storecheckout(Request $request)
    {

        $rule = [
        	'nama_depan' => 'required',
            'nama_belakang'=> 'required',
            'alamat'=> 'required',
            'kota'=> 'required',
            'kodepos'=> 'required',
            'telepon'=> 'required',
            'email'=> 'required'
        ];
        $this->validate($request, $rule);

        $input = $request->all();
        $checkout =  new \App\Checkout;

        $checkout->nama_depan = $input['nama_depan'];
        $checkout->nama_belakang = $input['nama_belakang'];
        $checkout->kota = $input['kota'];
        $checkout->kodepos = $input['kodepos'];
        $checkout->telepon = $input['telepon'];
        $checkout->email = $input['email'];

        $do = new Checkout($request->all());
        $do->save();

        return redirect('/detailcheckout');
    }

    public function detailcheckout()
    {
    	$data['produk'] = \DB::table('t_cart')->get();
    	$data['checkout'] = \DB::table('t_checkout')->get();
    	$data['subtotal'] = \DB::table('t_cart')->sum('harga');
    	$data['cart'] = Cart::count();
        return view('user/detailcheckout', $data);
    }
}
