<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\Cart;

class BlogUserController extends Controller
{
     public function blog()
    {
       $data['berita'] = Berita::get();
       $data['cart'] = Cart::count();
        return view('user/blog', $data);
    }
}
