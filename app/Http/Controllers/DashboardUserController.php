<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Berita;
use App\Banner;
use App\Kategori;
use App\Cart;

class DashboardUserController extends Controller
{
     public function naparell()
    {
    	$data['banner'] = Banner::get();
        $data['cart'] = Cart::count();
        $data['shop'] = Produk::get();
        return view('user/naparell', $data);
    }
}
