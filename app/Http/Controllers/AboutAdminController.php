<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutAdminController extends Controller
{
     public function aboutadmin()
    {
    	return view('admin/about');
    }
}
