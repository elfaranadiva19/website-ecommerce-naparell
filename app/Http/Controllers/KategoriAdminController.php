<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;

class KategoriAdminController extends Controller
{
     public function kategori()
    {
       $data['kategori'] = \DB::table('t_kategori')->get();
        return view('admin/kategoriproduk', $data);
    }

      public function createkategori()
    {
        return view('admin/form/formkategori');
    }

    public function storekategori(Request $request)
    {

        $rule = [
            'nama'=> 'required'
        ];
        $this->validate($request, $rule);

        $input = $request->all();
        $kategori =  new \App\Kategori;

        $kategori->nama = $input['nama'];

        $do = new Kategori($request->all());
        $do->save();

        return redirect('/kategoriproduk');
    }

     public function editkategori(Request $request, $id)
    {
        $data['kategori'] = \DB::table('t_kategori')->find($id);
        return view('admin/form/formkategori', $data);
    }

    public function updatekategori(Request $request, $id)
    {
         $rule = [
            'nama' => 'required'
        ];
        $this->validate($request, $rule);

        $input = $request->all();

        $kategori = \App\Kategori::find($id);

        $kategori->nama = $input['nama'];

        $kategori->update();

        return redirect ('/kategoriproduk');

    }

    public function hapuskategori($id)
        {
        // hapus data
        Kategori::where('id',$id)->delete();
        return redirect('/kategoriproduk');
        }
}
