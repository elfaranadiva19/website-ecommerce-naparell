<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Produk;


class MessageController extends Controller
{
    public function message()
    {
    	$data['produk'] = \DB::table('t_cart')->get();
    	$data['checkout'] = \DB::table('t_checkout')->get();
    	$data['cart'] = Cart::count();
        return view('user/message', $data);
    }
}
