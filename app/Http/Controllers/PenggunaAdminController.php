<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PenggunaAdminController extends Controller
{
     public function pengguna()
    {
    	$data['pengguna'] = \DB::table('users')->get();
        return view('admin/pengguna', $data);
    }
}
