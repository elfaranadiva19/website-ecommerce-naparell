<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\Cart;

class detBlogUserController extends Controller
{
        public function showblog($id)
    {
      $data['berita'] = Berita::find($id);
      $data['cart'] = Cart::count();
        return view('user/detailblog', $data);
    }

}
