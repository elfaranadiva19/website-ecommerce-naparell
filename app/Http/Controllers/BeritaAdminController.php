<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use File;

class BeritaAdminController extends Controller
{
     public function berita()
    {
    	$data['berita'] = \DB::table('t_berita')->get();
        return view('admin/berita', $data);
    }

      public function createberita()
    {
    	return view('admin/form/formberita');
    }

	public function storeberita(Request $request)
    {

        $rule = [
            'foto' => 'required|file|image|mimes:jpg,png,jpeg,svg|max:2048',
            'judul'=> 'required',
            'deskripsi'=> 'required'
        ];
        $this->validate($request, $rule);

        $input = $request->all();

        $berita =  new \App\Berita;

        $berita->foto = $input['foto'];
        $berita->judul = $input['judul'];
        $berita->deskripsi = $input['deskripsi'];

        $file = $request->file('foto');
        $namefile = $file->getClientOriginalName();
        $request->file('foto')->move('upload/berita', $namefile);
        $do = new Berita($request->all());
        $do->foto = $namefile;
        $do->save();

        return redirect('/berita');
    }

     public function editberita(Request $request, $id)
    {
        $data['berita'] = \DB::table('t_berita')->find($id);
        return view('admin/form/formberita', $data);
    }

    public function updateberita(Request $request, $id)
    {
         $rule = [
            'foto' => 'required|file|image|mimes:jpg,png,jpeg,svg|max:2048',
            'judul' => 'required',
            'deskripsi'=> 'required'
        ];
        $this->validate($request, $rule);

        $input = $request->all();

        $berita = \App\Berita::find($id);

        if($request->file('foto') == "")
        {
            $berita->foto = $berita->foto;

        }else {

            $file = $request->file('foto');
            $namefile = $file->getClientOriginalName();
            $request->file('foto')->move('upload/berita', $namefile);
            $berita->foto = $namefile;
        }
        $berita->judul = $input['judul'];
        $berita->deskripsi = $input['deskripsi'];

        $berita->update();

        return redirect ('/berita');

	}

	public function hapusberita($id)
        {
        // hapus file
        $berita = Berita::where('id',$id)->first();
        File::delete('upload/berita/'.$berita->foto);
 
        // hapus data
        Berita::where('id',$id)->delete();
        return redirect('/berita');
        }

     public function detailberitaad($id)
    	{
       $news = Berita::find($id);
        return view('admin/detailberitaad', compact('news'));
    }
}
