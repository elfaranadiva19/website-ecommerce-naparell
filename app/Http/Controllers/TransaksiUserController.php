<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Produk;
use App\Checkout;
use App\Transaksi;


class TransaksiUserController extends Controller
{
    public function transaksiuser()
    {
    	$data['produk'] = \DB::table('t_cart')->get();
    	$data['checkout'] = \DB::table('t_checkout')->get();
    	$data['cart'] = Cart::count();
        return view('user/transaksi', $data);
    }

       public function addtransaksi($id){
    	$checkout = Checkout::find($id);
    	Transaksi::create([
    		'email' => $checkout->email,
    	]);

    	return redirect('/message');
    }
}
