<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InboxAdminController extends Controller
{
    public function inbox()
    {
    	return view('admin/inbox');
    }
}
