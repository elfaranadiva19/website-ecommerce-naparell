<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use File;

class BannerAdminController extends Controller
{
     public function banner()
    {
    	$data['banner'] = \DB::table('t_banner')->get();
        return view('admin/banner', $data);
    }

      public function createbanner()
    {
    	return view('admin/form/formbanner');
    }

     public function storebanner(Request $request)
    {

        $rule = [
            'foto' => 'required|file|image|mimes:jpg,png,jpeg,svg|max:2048',
            'deskripsi'=> 'required'
        ];
        $this->validate($request, $rule);

        $input = $request->all();

        $banner =  new \App\Banner;

        $banner->foto = $input['foto'];
        $banner->deskripsi = $input['deskripsi'];

        $file = $request->file('foto');
        $namefile = $file->getClientOriginalName();
        $request->file('foto')->move('upload/banner', $namefile);
        $do = new Banner($request->all());
        $do->foto = $namefile;
        $do->save();

        return redirect('/banner');
    }

    public function editbanner(Request $request, $id)
    {
        $data['banner'] = \DB::table('t_banner')->find($id);
        return view('admin/form/formbanner', $data);
    }

    public function updatebanner(Request $request, $id)
    {
         $rule = [
            'foto' => 'required|file|image|mimes:jpg,png,jpeg,svg|max:2048',
            'deskripsi'=> 'required'
        ];
        $this->validate($request, $rule);

        $input = $request->all();

        $banner = \App\Banner::find($id);

        if($request->file('foto') == "")
        {
            $banner->foto = $banner->foto;

        }else {

            $file = $request->file('foto');
            $namefile = $file->getClientOriginalName();
            $request->file('foto')->move('upload/banner', $namefile);
            $banner->foto = $namefile;
        }

        $banner->deskripsi = $input['deskripsi'];

        $banner->update();

        return redirect ('/banner');

	}

	public function hapusbanner($id)
        {
        // hapus file
        $banner = Banner::where('id',$id)->first();
        File::delete('upload/banner/'.$banner->foto);
 
        // hapus data
        Banner::where('id',$id)->delete();
        return redirect('/banner');
        }
}
