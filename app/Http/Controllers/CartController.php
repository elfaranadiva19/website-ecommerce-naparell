<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Produk;

class CartController extends Controller
{
    public function cart(){
        $data['produk'] = \DB::table('t_cart')->get();
    	$data['subtotal'] = \DB::table('t_cart')->sum('harga');
    	$data['cart'] = Cart::count();
        return view('user/cart', $data);
    }

    public function add($id){
    	$produk = Produk::find($id);
    	Cart::create([
    		'foto' => $produk->foto,
    		'nama' => $produk->nama,
    		'qty' =>  1,
    		'harga' => $produk->harga,
    	]);

    	return redirect()->back();
    }

    public function hapuscart($id)
        {
 
        // hapus data
        Cart::where('id',$id)->delete();
        return redirect('/cart');
        }

}


