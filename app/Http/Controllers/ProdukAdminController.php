<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use File;

class ProdukAdminController extends Controller
{
     public function produk()
    {
    	$data['produk'] = \DB::table('t_produk')->get();
        return view('admin/produk', $data);
    }

     public function createproduk()
    {
    	return view('admin/form/formproduk');
    }

    public function storeproduk(Request $request)
    {

        $rule = [
            'foto' => 'required|file|image|mimes:jpg,png,jpeg,svg|max:2048',
            'nama' => 'required|string',
            'jenis' => 'required',
            'jumlah' => 'required',
            'harga' =>'required',
            'deskripsi'=> 'required'
        ];
        $this->validate($request, $rule);

        $input = $request->all();

        $produk =  new \App\Produk;

        $produk->foto = $input['foto'];
        $produk->nama = $input['nama'];
        $produk->jenis = $input['jenis'];
        $produk->jumlah = $input['jumlah'];
        $produk->harga = $input['harga'];
        $produk->deskripsi = $input['deskripsi'];

        $file = $request->file('foto');
        $namefile = $file->getClientOriginalName();
        $request->file('foto')->move('upload/produk', $namefile);
        $do = new Produk($request->all());
        $do->foto = $namefile;
        $do->save();

        return redirect('/produk');
    }

     public function editproduk(Request $request, $id)
    {
        $data['produk'] = \DB::table('t_produk')->find($id);
        return view('admin/form/formproduk', $data);
    }

    public function updateproduk(Request $request, $id)
    {
         $rule = [
            'foto' => 'required|file|image|mimes:jpg,png,jpeg,svg|max:2048',
            'nama' => 'required|string',
            'jenis' => 'required',
            'jumlah' => 'required',
            'harga' =>'required',
            'deskripsi'=> 'required'
        ];
        $this->validate($request, $rule);

        $input = $request->all();

        $produk = \App\Produk::find($id);

        if($request->file('foto') == "")
        {
            $produk->foto = $produk->foto;

        }else {

            $file = $request->file('foto');
            $namefile = $file->getClientOriginalName();
            $request->file('foto')->move('upload/produk', $namefile);
            $produk->foto = $namefile;
        }

        $produk->nama = $input['nama'];
        $produk->jenis = $input['jenis'];
        $produk->jumlah = $input['jumlah'];
        $produk->harga = $input['harga'];
        $produk->deskripsi = $input['deskripsi'];

        $produk->update();

        return redirect ('/produk');

	}

	public function hapusproduk($id)
        {
        // hapus file
        $produk = Produk::where('id',$id)->first();
        File::delete('upload/produk/'.$produk->foto);
 
        // hapus data
        Produk::where('id',$id)->delete();
        return redirect('/produk');
        }

    public function detailproduk($id)
    {
       $po = Produk::find($id);
        return view('admin/detailproduk', compact('po'));
    }
}
