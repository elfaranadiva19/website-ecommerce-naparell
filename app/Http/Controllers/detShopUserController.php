<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Cart;

class detShopUserController extends Controller
{
       public function show($id)
    {
        $data['model'] = Produk::find($id);
        $data['cart'] = Cart::count();
        return view('user/detailshop', $data);
    }
}
