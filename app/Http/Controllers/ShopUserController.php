<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Kategori;
use App\Cart;

class ShopUserController extends Controller
{
     public function shop(Request $request)
    {
    	$data['shop'] = Produk::paginate(2);
        $data['cart'] = Cart::count();
        $data['kategori'] = Kategori::get();
        return view('user/shop', $data);
    }

}
