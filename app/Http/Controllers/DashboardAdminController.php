<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Banner;
use App\Berita;
use App\Kategori;

class DashboardAdminController extends Controller
{
     public function dashboard()
    {
        $data ['count'] = Produk::count();
        $data ['jumlah'] = Berita::count();
        $data ['banner'] = Banner::count();
        $data ['kategori'] = Kategori::count();
        return view('admin/dashboard', $data);
    }
}
