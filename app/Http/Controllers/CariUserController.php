<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Kategori;
use App\Cart;

class CariUserController extends Controller
{
    public function cari(Request $request)
    {
        // menangkap data pencarian
        $cari = $request->get('cari');
        $data['cart'] = Cart::count();
 
            // mengambil data dari table pegawai sesuai pencarian data
        $data['shop'] = Produk::where('nama','like',"%".$cari."%")
        ->orwhere('jenis','like',"%".$cari."%")->paginate();
        $data['kategori'] = Kategori::paginate();
 
            // mengirim data pegawai ke view index
        return view('user/shop', $data);
 
    }
}
