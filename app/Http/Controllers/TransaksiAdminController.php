<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;

class TransaksiAdminController extends Controller
{
     public function transaksi()
    {
    	$data['transaksi'] = \DB::table('transaksi')->get();
    	return view('admin/transaksi', $data);
    }
}
