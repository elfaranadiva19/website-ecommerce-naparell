<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Kategori;
use App\Cart;

class FilterShopUserController extends Controller
{
      public function fltShop(Request $request){
        $nama = $request->nama;
        $data['cart'] = Cart::count();
        $data['shop'] = \DB::table('t_produk')->where('jenis', '=', $nama)->paginate(2);
        $data['kategori'] = \DB::table('t_kategori')->get();
        return view('user/shop', $data);   
    }

}
