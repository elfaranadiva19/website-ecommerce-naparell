<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 't_kategori';
    protected $fillable = ['nama'];
}
