<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 't_banner';
    protected $fillable = ['foto','deskripsi'];
}
