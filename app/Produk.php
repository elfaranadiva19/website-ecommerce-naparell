<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 't_produk';
    protected $fillable = ['foto','nama','jenis','jumlah','harga','deskripsi'];
}
