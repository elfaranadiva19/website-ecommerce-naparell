<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $table = 't_checkout';
    protected $fillable = ['nama_depan','nama_belakang','alamat','kota','kodepos','telepon','email'];
}
