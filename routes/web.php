<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/berita/detailberitaad/{id}', 'BeritaAdminController@detailberitaad');
Route::get('/produk/detailproduk/{id}', 'ProdukAdminController@detailproduk');
Route::get('/dashboard', 'DashboardAdminController@dashboard');
//////////////////////////////////////////////
Route::get('/produk', 'ProdukAdminController@produk');
Route::get('/produk/createproduk', 'ProdukAdminController@createproduk');
Route::post('/produk', 'ProdukAdminController@storeproduk');
Route::get('/produk/editproduk/{edit}', 'ProdukAdminController@editproduk');
Route::patch('/produk/{id}', 'ProdukAdminController@updateproduk');
Route::get('/produk/hapusproduk/{id}','ProdukAdminController@hapusproduk');
/////////////////////////////////////////////
Route::get('/banner', 'BannerAdminController@banner');
Route::get('/banner/createbanner', 'BannerAdminController@createbanner');
Route::post('/banner', 'BannerAdminController@storebanner');
Route::get('/banner/editbanner/{edit}', 'BannerAdminController@editbanner');
Route::patch('/banner/{id}', 'BannerAdminController@updatebanner');
Route::get('/banner/hapusbanner/{id}','BannerAdminController@hapusbanner');
/////////////////////////////////////////////
Route::get('/berita', 'BeritaAdminController@berita');
Route::get('/berita/createberita', 'BeritaAdminController@createberita');
Route::post('/berita', 'BeritaAdminController@storeberita');
Route::get('/berita/editberita/{edit}', 'BeritaAdminController@editberita');
Route::patch('/berita/{id}', 'BeritaAdminController@updateberita');
Route::get('/berita/hapusberita/{id}','BeritaAdminController@hapusberita');
/////////////////////////////////////////////
Route::get('/kategoriproduk', 'KategoriAdminController@kategori');
Route::get('/kategoriproduk/createkategori', 'KategoriAdminController@createkategori');
Route::post('/kategoriproduk', 'KategoriAdminController@storekategori');
Route::get('/kategoriproduk/editkategori/{edit}', 'KategoriAdminController@editkategori');
Route::patch('/kategoriproduk/{id}', 'KategoriAdminController@updatekategori');
Route::get('/kategoriproduk/hapuskategori/{id}','KategoriAdminController@hapuskategori');
/////////////////////////////////////////////
Route::get('/inbox', 'InboxAdminController@inbox');
Route::get('/pengguna', 'PenggunaAdminController@pengguna');
Route::get('/aboutadmin', 'AboutAdminController@aboutadmin');
Route::get('/transaksi', 'TransaksiAdminController@transaksi');
////////////////////////////////////////////////////////////////////////////////////////////
Route::get('/naparell', 'DashboardUserController@naparell');
Route::get('/shop', 'ShopUserController@shop');
Route::get('/shop/detail/{id}', 'detShopUserController@show');
Route::get('/blog', 'BlogUserController@blog');
Route::get('/blog/{id}', 'detBlogUserController@showblog');
Route::get('/about', 'AboutUserController@about');
Route::get('/contact', 'ContactUserController@contact');

Route::get('/shop/{nama}', 'FilterShopUserController@fltShop');
Route::get('/cari','CariUserController@cari');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cart', 'CartController@cart');
Route::get('/cart/hapuscart/{id}', 'CartController@hapuscart');
Route::post('/in/{id}', 'CartController@add');
Route::get('/formcheckout', 'ChekController@formcheckout');
Route::post('/formcheckout', 'ChekController@storecheckout');
Route::get('/detailcheckout', 'ChekController@detailcheckout');
Route::get('/transaksiuser', 'TransaksiUserController@transaksiuser');
Route::post('/on/{id}', 'TransaksiUserController@addtransaksi');
Route::get('/message', 'MessageController@message');


