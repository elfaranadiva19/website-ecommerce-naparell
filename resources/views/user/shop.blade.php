@extends('layoutuser.content')

@section('content')



<div class="hero-wrap hero-bread" style="background-image: url('{{asset('/asset/images/fashion 1.jpg')}}');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Products</span></p>
            <h1 class="mb-0 bread">Products</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section">
    	<div class="container">
    		<div class="row justify-content-center">
    			<div class="col-md-10 mb-5 text-center">
    				<ul class="product-category">
    					<li><a href="{{url('/shop')}}">All</a></li>
                        @foreach ($kategori as $row)
                        <li><a href="{{url('/shop/'.$row->nama)}}" name="jenis">{{ $row->nama }}</a></li>
                        @endforeach
    				</ul>
    			</div>
    		</div>
    		<div class="row">
    			    @foreach( $shop as $s )
    			<div class="col-md-6 col-lg-3 ftco-animate">
    				<div class="product">
    					<a href="{{ url('/shop/detail/'.$s->id) }}" class="img-prod" >
    						<img class="img-fluid" src="{{ url('/upload/produk/'.$s->foto) }}" style="height: 310px; width: 480px" alt="Colorlib Template">
    						<div class="overlay"></div>
    					</a>
    					<div class="text py-3 pb-4 px-3 text-center">
    						<h3><a href="#">{{$s->nama}}</a></h3>
                            <h7>{{$s->jenis}}</h7>
    						<div class="d-flex">
    							<div class="pricing">
		    						<span class="price-sale">{{$s->harga}}</span></p>
		    					</div>
	    					</div>
	    					<div class="bottom-area d-flex px-3">
	    						<div class="m-auto d-flex">
	    							<button class="btn btn-primary">
	    								<span><i class="ion-ios-menu"></i></span>
	    							</button>
                                    <form action="{{ url('in/'. $s->id) }}" method="post">
                                        @csrf
                                        <button class="btn btn-primary" type="submit"><i class="ion-ios-cart"></i></button>
                                    </form>
	    							<button class="btn btn-primary">
	    								<span><i class="ion-ios-heart"></i></span>
	    							</button>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    			@endforeach
    		</div>
            {{$shop->links()}}
    	</div>
    </section>

@endsection