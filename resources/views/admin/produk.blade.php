@extends('layoutadmin.content')

@section('content')
          <div class="content-wrapper">
          	 <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-cart"></i>
                </span> Produk</h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                  </li>
                </ul>
              </nav>
            </div>
            <div class="col-lg-15 grid-margi">
                <div class="card table-responsive no-padding">
                  <div class="card-body">
                    <h4 class="card-title">Tabel Produk</h4>
                    <p class="card-description"><a href="{{ url('/produk/createproduk') }}" type="button" class="btn btn-gradient-danger btn-rounded btn-fw">Tambah Data</a>
                    </p>
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Foto</th>
                          <th>Nama</th>
                          <th>Jenis</th>
                          <th>Opsi</th>
                        </tr>
                      </thead>
                      <tbody>
                         @foreach($produk as $p)
                      <tr>
                        <td>{{ isset($i) ? ++$i : $i = 1}}</td>
                        <td><img src="{{ url('/upload/produk/'.$p->foto) }}"></td>
                        <td>{{ $p->nama }}</td>
                        <td>{{ $p->jenis}}</td>
                        <td>
                          <a class="badge badge-success"  href="{{ url('/produk/detailproduk/'.$p->id) }}">Show</a>
                          <a class="badge badge-success" href="{{url ('/produk/editproduk/'.$p->id) }}">Edit</a>
                          <a class="badge badge-danger" href="{{url ('/produk/hapusproduk/'.$p->id) }}">Hapus</a>
                        </td>
                      </tr>
                          @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
@endsection