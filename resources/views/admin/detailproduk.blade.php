@extends('layoutadmin.content')

@section('content')
 <div class="content-wrapper">
<div class="card my-4">
          <h5 class="card-header">Detail Produk</h5>
          <div class="card-body">
            <form>
              <div class="form-group">
                <h5>Foto Produk</h5>
                <p><img class="img-fluid rounded" src="{{ url('/upload/produk/'.$po->foto) }}" style="width: 60%; height: 65%;"  alt=" " ></p>
              </div>
              <div class="form-group">
                <h5>Nama Produk</h5>
                <p>{{$po->nama}}</p>
              </div>
              <div class="form-group">
                <h5>Jenis Produk</h5>
                <p>{{$po->jenis}}</p>
              </div>
               <div class="form-group">
                <h5>Jumlah Produk</h5>
                <p>{{$po->jumlah}}</p>
              </div>
               <div class="form-group">
                <h5>Harga Produk</h5>
                <p>{{$po->harga}}</p>
              </div>
              <div class="form-group">
                <h5>Deskripsi Produk</h5>
                <p>{!!$po->deskripsi!!}</p>
              </div>
            </form>
          </div>
        </div>
      </div>

@endsection