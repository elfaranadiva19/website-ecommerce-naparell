@extends('layoutadmin.content')

@section('content')
          <div class="content-wrapper">
          	<div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-cash-multiple"></i>
                </span> Transaksi</h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                  </li>
                </ul>
              </nav>
            </div>
              <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Transaksi</h4>
                    <p class="card-description">
                    </p>
                    <table class="table table-striped">
                    <thead>
                        <tr>
                          <th>No</th>
                          <th>Email</th>
                        </tr>
                      </thead>
                      <tbody>
                         @foreach($transaksi as $p)
                      <tr>
                        <td>{{ isset($i) ? ++$i : $i = 1}}</td>
                        <td>{{ $p->email }}</td>
                      </tr>
                          @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
          </div>
@endsection