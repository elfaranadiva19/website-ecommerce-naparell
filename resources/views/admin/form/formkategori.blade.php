@extends('layoutadmin.content')

@section('content')
          <div class="content-wrapper">
          	 <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-label"></i>
                </span>Form Kategori</h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                  </li>
                </ul>
              </nav>
            </div>
         <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Form Input Data</h4>
                    <form class="forms-sample" action="{{ url('kategoriproduk', @$kategori->id) }}" method="post" enctype="multipart/form-data">
                      @csrf
                      
                      @if(!empty($kategori))
                      @method('PATCH')
                      @endif
                       <div class="form-group">
                        <label>Nama Kategori</label>
                        <input type="text" class="form-control" placeholder="Nama Produk" name="nama" value="{{ old('nama', @$kategori->nama) }}">
                      </div>
                      <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                      <a href="{{url('/kategoriproduk')}}" class="btn btn-light">Cancel</a>
                    </form>
                  </div>
                </div>
              </div>
          </div>
@endsection