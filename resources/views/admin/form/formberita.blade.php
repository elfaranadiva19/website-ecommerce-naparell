@extends('layoutadmin.content')

@section('content')
          <div class="content-wrapper">
          	 <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-newspaper"></i>
                </span>Form Berita</h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                  </li>
                </ul>
              </nav>
            </div>
         <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Form Input Data</h4>
                    <form class="forms-sample" action="{{ url('berita', @$berita->id) }}" method="post" enctype="multipart/form-data">
                      @csrf
                      
                      @if(!empty($berita))
                      @method('PATCH')
                      @endif
                       <div class="form-group">
                        <label>File upload</label>
                        <input type="file" name="foto" class="file-upload-default">
                        <div class="input-group col-xs-12">
                          <input type="file" class="form-control file-upload-info" name="foto">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Judul</label>
                        <input type="text" class="form-control" placeholder="Nama Produk" name="judul" value="{{ old('judul', @$berita->judul) }}">
                      </div>
                      <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="deskripsi" class="ckeditor">{{ old('deskripsi', @$berita->deskripsi) }}</textarea>
                      </div>
                      <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                      <a href="{{url('/berita')}}" class="btn btn-light">Cancel</a>
                    </form>
                  </div>
                </div>
              </div>
          </div>
@endsection