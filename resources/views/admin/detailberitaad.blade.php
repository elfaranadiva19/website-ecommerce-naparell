@extends('layoutadmin.content')

@section('content')
  <div class="content-wrapper">
  <!-- Post Content Column -->
      <div class="col-lg-8">

        <!-- Title -->
        <h1 class="mt-4">{{$news->judul}}</h1>

        <!-- Author -->
        <p class="lead">
          by
          <a href="#">Start Bootstrap</a>
        </p>

        <hr>

        <!-- Date/Time -->
        <p>Posted on January 1, 2019 at 12:00 PM</p>

        <hr>

        <!-- Preview Image -->
        <img class="img-fluid rounded" src="{{ url('/upload/berita/'.$news->foto) }}" alt="">

        <hr>

        <!-- Post Content -->
        <p class="lead">{!!$news->deskripsi!!}</p>


        <hr>
     
      </div>

</div>

  @endsection