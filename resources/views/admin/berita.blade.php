@extends('layoutadmin.content')

@section('content')
          <div class="content-wrapper">
          	 <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-newspaper"></i>
                </span> Berita</h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                  </li>
                </ul>
              </nav>
            </div>
            <div class="col-lg-15 grid-margi">
                <div class="card table-responsive no-padding">
                  <div class="card-body">
                    <h4 class="card-title">Tabel Berita</h4>
                    <p class="card-description"><a href="{{ url('/berita/createberita') }}" type="button" class="btn btn-gradient-danger btn-rounded btn-fw">Tambah Data</a>
                    </p>
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Foto</th>
                          <th>Judul</th>
                          <th>Opsi</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($berita as $n)
                      <tr>
                        <td>{{ isset($i) ? ++$i : $i = 1}}</td>
                        <td><img src="{{ url('/upload/berita/'.$n->foto) }}"></td>
                        <td>{{ $n->judul}}</td>
                        <td>
                          <a class="badge badge-success" href="{{ url('/berita/detailberitaad/'.$n->id) }}">Show</a>
                          <a class="badge badge-success" href="{{url ('/berita/editberita/'.$n->id) }}">Edit</a>
                          <a class="badge badge-danger" href="{{url ('/berita/hapusberita/'.$n->id) }}">Hapus</a>
                        </td>
                      </tr>
                          @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
@endsection