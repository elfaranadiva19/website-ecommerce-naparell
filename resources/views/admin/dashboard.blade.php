@extends('layoutadmin.content')

@section('content')
          <div class="content-wrapper">
          	<div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-home"></i>
                </span> Dashboard </h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                  </li>
                </ul>
              </nav>
            </div>
                <div class="row">
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-danger card-img-holder text-white">
                  <div class="card-body">
                    <img src="{{asset ('/images/dashboard/circle.svg') }}" class="card-img-absolute" alt="circle-image">
                    <h4 class="font-weight-normal mb-3">Produk<i class="mdi mdi-cart mdi-24px float-right"></i>
                    </h4>
                    <h2 class="mb-5">{{$count}}</h2>
                    <a style="color: #fff;" href="{{url('/produk')}}">
                  <span class="menu-title">More Info</span>
                  <i class="mdi mdi-arrow-right-bold-circle menu-icon"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-info card-img-holder text-white">
                  <div class="card-body">
                    <img src="{{asset ('/images/dashboard/circle.svg') }}" class="card-img-absolute" alt="circle-image">
                    <h4 class="font-weight-normal mb-3">Berita<i class="mdi mdi-newspaper mdi-24px float-right"></i>
                    </h4>
                    <h2 class="mb-5">{{$jumlah}}</h2>
                    <a style="color: #fff;" href="{{url('/berita')}}">
                  <span class="menu-title">More Info</span>
                  <i class="mdi mdi-arrow-right-bold-circle menu-icon"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-success card-img-holder text-white">
                  <div class="card-body">
                    <img src="{{asset ('/images/dashboard/circle.svg') }}" class="card-img-absolute" alt="circle-image">
                    <h4 class="font-weight-normal mb-3">Banner<i class="mdi mdi-image mdi-24px float-right"></i>
                    </h4>
                    <h2 class="mb-5">{{$banner}}</h2>
                    <a style="color: #fff;" href="{{url('/banner')}}">
                  <span class="menu-title">More Info</span>
                  <i class="mdi mdi-arrow-right-bold-circle menu-icon"></i>
                    </a>
                  </div>
                </div>
              </div>
               <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-danger card-img-holder text-white">
                  <div class="card-body">
                    <img src="{{asset ('/images/dashboard/circle.svg') }}" class="card-img-absolute" alt="circle-image">
                    <h4 class="font-weight-normal mb-3">Kategori Produk<i class="mdi mdi-label mdi-24px float-right"></i>
                    </h4>
                    <h2 class="mb-5">{{$kategori}}</h2>
                    <a style="color: #fff;" href="{{url('/kategoriproduk')}}">
                  <span class="menu-title">More Info</span>
                  <i class="mdi mdi-arrow-right-bold-circle menu-icon"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
@endsection