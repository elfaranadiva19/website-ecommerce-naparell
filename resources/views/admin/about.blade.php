@extends('layoutadmin.content')

@section('content')
          <div class="content-wrapper">
          	 <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-credit-card-multiple"></i>
                </span>About</h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                  </li>
                </ul>
              </nav>
            </div>
            <div class="col-lg-15 grid-margi">
                <div class="card table-responsive no-padding">
                  <div class="card-body">
                    <h4 class="card-title">Tabel About</h4>
                    <p class="card-description"><a href="{{ url('/aboutadmin/createabout') }}" type="button" class="btn btn-gradient-danger btn-rounded btn-fw">Tambah Data</a>
                    </p>
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Foto</th>
                          <th>Judul</th>
                          <th>Deskripsi</th>
                          <th>Opsi</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
@endsection