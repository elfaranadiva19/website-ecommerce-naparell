 <style type="text/css">

.form {
    width:400px;
    margin:11px auto;
}
.search {
    padding:4px 14px;
    background:rgba(50, 50, 50, 0.2);
    border:0px solid #dbdbdb;
    border-radius: 20px;
}

 </style>

 <body class="goto-here">
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="{{url('/home')}}">
	      <img src="{{asset('/asset/images/logoadm.png')}}" alt="Image" class="img-fluid"></a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>


	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	        <li>
				<form action="{{url('/cari')}}" class="form" method="GET">
					<input type="search" class="search" name="cari" placeholder="cari produk">
					</form></li>
	          <li class="nav-item"><a href="{{url('/home')}}" class="nav-link">Home</a></li>
	          <li class="nav-item"><a href="{{url('/shop')}}" class="nav-link">Shop</a></li>
	          <li class="nav-item"><a href="{{url('/about')}}" class="nav-link">About</a></li>
	          <li class="nav-item"><a href="{{url('/blog')}}" class="nav-link">Blog</a></li>
	          <li class="nav-item"><a href="{{url('/contact')}}" class="nav-link">Contact</a></li>
	          <li class="nav-item"><a href="cart.html" class="nav-link"><span class="icon-shopping_cart"></span>[0]</a></li>

	        </ul>
	      </div>
	    </div>
	  </nav>