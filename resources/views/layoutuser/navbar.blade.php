 <style type="text/css">

.form {
    width:240px;
    margin:11px auto;
}
.search {
    padding:4px 14px;
    background:rgba(50, 50, 50, 0.2);
    border:0px solid #dbdbdb;
    border-radius: 20px;
}

 </style>

 <body class="goto-here">
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">

        </div>
	    <div class="container">
	      <a class="navbar-brand" href="{{url('/naparell')}}">
	      <img src="{{asset('/asset/images/logoadm.png')}}" alt="Image" class="img-fluid"></a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	          <li class="nav-item"><a href="{{url('/naparell')}}" class="nav-link">Home</a></li>
	          <li class="nav-item"><a href="{{url('/shop')}}" class="nav-link">Shop</a></li>
	          <li class="nav-item"><a href="{{url('/about')}}" class="nav-link">About</a></li>
	          <li class="nav-item"><a href="{{url('/blog')}}" class="nav-link">Blog</a></li>
	          <li class="nav-item"><a href="{{url('/contact')}}" class="nav-link">Contact</a></li>
	          <li class="nav-item"><a href="{{url('/cart')}}" class="nav-link"><span class="icon-shopping_cart"></span>[{{$cart}}]</a></li>
	          <li>
				<form action="{{url('/cari')}}" class="form" method="GET">
					<input type="search" class="search" name="cari" placeholder="cari produk">
					</form></li>
			@guest
				<li class="nav-item">
					<a class="nav-link" href="{{ route('login') }}">{{__('Login')}}</a>
				</li>
				@if (Route::has('register'))
				<li class="nav-item" >
					<a class="nav-link" href="{{ route('register') }}">{{__('Register')}}</a>
				</li>
				@endif
			@else
				<li class="nav-item dropdown">
					<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
					data-togglr="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
					{{ Auth::user()->name }} <span class="caret"></span>
				</a>

				<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="{{ route('logout') }}"
					onclick="event.preventDefault();
								document.getElementById('logout-form').submit();">
					{{__('Logout') }}
				</a>

				<form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
					@csrf
				</form>
			</div>
		</li>
		@endguest
			 </ul>
	      </div>
	    </div>
	  </nav>